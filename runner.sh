#!/bin/bash
echo "--------------------------------------------------------------------"
echo "-------------H E L L O    D E V E L O P E R S-----------------------"
echo "--------------------------------------------------------------------"
echo "-----We offer these tools for you lab practices :\
	1. Jenkins \
	2. Bitbucket---------"
echo "--------------------------------------------------------------------"
echo "--------------------------------------------------------------------"
echo "-----------Please enter the tool name you want to setup-------------"
echo "--------------------------------------------------------------------"
echo "--------------------------------------------------------------------"
echo "--------------------------------------------------------------------"
read tool
if [$tool = 'Jenkins']
then
	cd Jenkins
	sh spinup-container.sh
elif [$tool = 'Bitbucket']
then
	cd Bitbucket
	sh spinup.sh
else
	echo "Enter a valid tool name"
fi
