#!/bin/bash
echo ".............................................................."
echo ".............................................................."
echo "............Creating New Docker network ......................"
echo ".............................................................."
echo ".............................................................."
docker network create --driver bridge --subnet=172.18.0.0/16 myBitbucketNetwork
echo ".............................................................."
echo ".............................................................."
echo "............Building New Docker Image........................"
echo ".............................................................."
echo ".............................................................."
docker build -t atlassian/bitbucket .
echo ".............................................................."
echo ".............................................................."
echo "............Building New Docker Volume........................"
echo ".............................................................."
echo ".............................................................."
docker volume create --name bitbucketVolume
echo ".............................................................."
echo ".............................................................."
echo "............Running Docker conatiner with parameters.........."
echo ".............................................................."
echo ".............................................................."
docker run --network=myBitbucketNetwork --ip=172.18.1.1 \
    -e SEARCH_ENABLED=false \
    -e JDBC_DRIVER=org.postgresql.Driver \
    -e JDBC_USER=atlbitbucket \
    -e JDBC_PASSWORD=MYPASSWORDSECRET \
    -e JDBC_URL=jdbc:postgresql://my.database.host:5432/bitbucket \
    -e PLUGIN_SEARCH_CONFIG_BASEURL=http://my.opensearch.host \
    -v /data/bitbucket-shared:/var/atlassian/application-data/bitbucket/shared \
    --name="bitbucket" \
    -d -p 7990:7990 -p 7999:7999 \
    atlassian/bitbucket
